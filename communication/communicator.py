class Communicator:
    def __init__(self, known_transactions=None, known_blockchain=None):
        pass

    def publish_transaction(self, transaction):
        pass

    def update_transactions(self):
        pass

    def publish_block(self, block):
        pass

    def update_blockchain(self):
        pass
