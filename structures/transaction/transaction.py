from bytable import Bytable, SingleAttributeDescription, AttributeTypes, IterableAttributeDescription

from consts import TRANSACTION, HASH
from structures.transaction.input import TransactionInput
from structures.transaction.output import TransactionOutput


class TransactionWithoutID(Bytable):
    attribute_descriptions = [
        IterableAttributeDescription(
            'inputs',
            SingleAttributeDescription('inputs__item', TransactionInput),
            max_length=TRANSACTION.MAX_INPUT_COUNT
        ),

        IterableAttributeDescription(
            'outputs',
            SingleAttributeDescription('outputs__item', TransactionOutput),
            max_length=TRANSACTION.MAX_OUTPUT_COUNT
        ),
    ]


class Transaction(Bytable):
    attribute_descriptions = [
        SingleAttributeDescription('transaction', TransactionWithoutID),
        SingleAttributeDescription('id', AttributeTypes.BYTES, length=HASH.LENGTH),
    ]

    @classmethod
    def generate_with_id(cls, inputs, outputs):
        transaction = TransactionWithoutID(inputs, outputs)
        return cls(transaction.hash(), transaction)

    @classmethod
    def coinbase(cls, reward):
        return cls.generate_with_id([TransactionInput.null_input(reward)], [TransactionOutput(reward)])

    @property
    def inputs(self):
        return self.transaction.inputs

    @property
    def outputs(self):
        return self.transaction.outputs

    def get_total_input_amount(self, blockchain):
        return sum(input_.get_used_output(blockchain=blockchain).amount for input_ in self.inputs)

    def get_total_output_amount(self):
        return sum(output.amount for output in self.outputs)
    
    def get_miner_fee(self, blockchain):
        return self.get_total_input_amount(blockchain) - sum(output.amount for output in self.outputs)

    def is_coinbase_transaction(self):
        """
        Returns whether or not this transaction is a Coinbase transaction
            That is the first transaction of the block which grants the miner JIMCoin "from thin air"
        :return:
        """
        return (
            len(self.inputs) == 1 and
            self.inputs[0].is_null_input()
        )

    def is_valid(self, blockchain, can_be_coinbase=False):
        return (can_be_coinbase and self.is_coinbase_transaction()) or (
                self.get_total_input_amount(blockchain=blockchain) >= self.get_total_output_amount() and
                not any(blockchain.is_output_used(input_.transaction_id, input_.output_id) for input_ in self.inputs) and
                all(input_.is_unlocks(input_.get_used_output(blockchain)) for input_ in self.inputs)
        )
