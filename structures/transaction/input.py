from rsa.transform import bytes2int
import rsa

from bytable import Bytable, SingleAttributeDescription, AttributeTypes
from consts import HASH, RSA, TRANSACTION


class TransactionInput(Bytable):
    """
    This is a reference to a spendable bunch of JIMCoin.
    Inputs are outputs that are "unlocked"
    """
    attribute_descriptions = [
        SingleAttributeDescription('transaction_id', AttributeTypes.BYTES, length=HASH.LENGTH),
        SingleAttributeDescription('output_id', AttributeTypes.INT, length=4),

        SingleAttributeDescription('unlocking_signature', AttributeTypes.BYTES, length=RSA.SIGNATURE_LENGTH),
    ]

    @classmethod
    def null_input(cls, amount):
        return cls(
            amount,
            b'\x00' * HASH.LENGTH,
            TRANSACTION.INPUTS.NULL_OUTPUT_ID,
        )

    @classmethod
    def unlock_from_output(cls, blockchain, transaction_id, output_id, private_key):
        return cls(
            transaction_id,
            output_id,
            unlocking_signature=rsa.sign(blockchain.get_transaction(transaction_id).pack(), private_key, HASH.METHOD)
        )

    def is_null_input(self):
        return (
            bytes2int(self.transaction_id) == 0 and
            self.output_id == TRANSACTION.INPUTS.NULL_OUTPUT_ID,
        )

    def get_used_output(self, blockchain):
        """
        Return the TransactionOutput object that we use for this input
        :param blockchain: `BlockChain` object :)
        :return:
        """
        return blockchain.get_output(self, self.transaction_id, self.output_id)

    def is_unlocks(self, transaction_bytes, output):
        """
        Returns whether or not the signature for this transaction input matches the key specified in the output
            Reminder - all outputs are protected by a public key, one needs a valid signature in order to turn such output in to an input (effectively
                                                                                                                                'using' the JIMCoin)
        :param transaction_bytes: `bytes` - The input message that is signed
        :param output: `TransactionOutput`
        :return: `bool`
        """
        return rsa.verify(transaction_bytes, self.unlocking_signature, output.locking_public_key) == HASH.METHOD
