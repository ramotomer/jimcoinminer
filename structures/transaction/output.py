from bytable import Bytable, SingleAttributeDescription, AttributeTypes
from consts import RSA


class TransactionOutput(Bytable):
    """
    The result of a transaction. A spendable amount of JIMCoin
    The only way to spend this JIMCoin is to unlock this Output and turn it into a new input
    One can unlock an output if they have the private-key matching the public-key which is attached to the output.
    """
    attribute_descriptions = [
        SingleAttributeDescription('amount',             AttributeTypes.FLOAT,     length=32),
        SingleAttributeDescription('locking_public_key', AttributeTypes.ASCII_STR, max_length=RSA.KEY.PUBLIC.MAX_LENGTH),
    ]
