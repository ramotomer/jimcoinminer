from datetime import datetime

from bytable import Bytable, SingleAttributeDescription, AttributeTypes, IterableAttributeDescription
from consts import COIN, BLOCK
from merkle_tree.tree import MerkleTree
from structures.transaction.transaction import Transaction
from rsa.transform import bytes2int


class BlockHeader(Bytable):
    attribute_descriptions = [
        SingleAttributeDescription("timestamp", AttributeTypes.DATETIME, length=8),
        SingleAttributeDescription("target", AttributeTypes.INT, length=32),
        SingleAttributeDescription("nonce", AttributeTypes.INT, length=BLOCK.NONCE_BYTE_SIZE),
        SingleAttributeDescription("hash_of_previous_block", AttributeTypes.BYTES, max_length=32),
        SingleAttributeDescription("miner_public_key", AttributeTypes.ASCII_STR, length=400),
        SingleAttributeDescription("transaction_tree_root", AttributeTypes.BYTES, length=32),
    ]

    def is_valid(self):
        """
        Returns whether or not the hash of the block is under the value of the target
        :return: `bool`
        """
        return bytes2int(self.hash()) < self.target


class Block(Bytable):
    attribute_descriptions = [
        SingleAttributeDescription('header', BlockHeader),
        IterableAttributeDescription(
            'transactions',
            SingleAttributeDescription('transactions__item', Transaction),
            max_length=BLOCK.MAX_TRANSACTIONS,
        )
    ]

    @classmethod
    def from_transactions(cls, target, hash_of_previous_block, miner_public_key, block_reward_and_fees, transactions, nonce=0, timestamp=None):
        timestamp = timestamp or datetime.now()
        transactions = [Transaction.coinbase(block_reward_and_fees)] + transactions

        return cls(
            BlockHeader(
                timestamp,
                target,
                nonce,
                hash_of_previous_block,
                miner_public_key,
                MerkleTree(transactions).root_node.data
            ),
            transactions,
        )

    def current_block_reward(self, blockchain):
        block_index = 0
        for block_index, block in enumerate(blockchain.blocks):
            if block == self:
                break
        if self not in blockchain.blocks:
            block_index += 1
        return blockchain.get_block_reward(block_index)

    def is_first_transaction_the_only_coinbase(self):
        return self.transactions[0].transaction.is_coinbase_transaction() and \
               (not any(transaction.transaction.is_coinbase_transaction() for transaction in self.transactions[1:]))

    def is_coinbase_transaction_valid(self, blockchain):
        coinbase_transaction_total_output = self.transactions[0].get_total_output_amount()
        sum_of_miner_fees = sum(transaction.get_miner_fee(blockchain) for transaction in self.transactions)
        current_block_reward = self.current_block_reward(blockchain=blockchain)

        return coinbase_transaction_total_output <= (current_block_reward + sum_of_miner_fees)

    def is_target_valid(self, blockchain):
        expected_target = blockchain.get_next_target(blockchain.blocks.index(self) if self in blockchain.blocks else None)
        return self.header.target == expected_target

    def is_valid(self, blockchain):
        return (
            MerkleTree.is_valid_root(self.header.transaction_tree_root, self.transactions) and
            self.header.is_valid() and
            self.is_target_valid(blockchain=blockchain) and
            self.is_first_transaction_the_only_coinbase() and
            self.is_coinbase_transaction_valid(blockchain=blockchain) and
            all(transaction.is_valid(blockchain=blockchain) for transaction in self.transactions[1:])
        )
