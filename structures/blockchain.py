from datetime import datetime

from structures.block import Block
from bytable import Bytable, IterableAttributeDescription, SingleAttributeDescription
from consts import BLOCK, COIN


class BlockChain(Bytable):
    attribute_descriptions = [
        IterableAttributeDescription(
            'blocks',
            SingleAttributeDescription('blocks__item', Block),
            max_length=BLOCK.CHAIN.MAX_LENGTH
        ),
    ]

    def is_valid(self):
        for previous_block, block in zip(self.blocks, self.blocks[1:]):
            if block.header.hash_of_previous_block != previous_block.hash():
                return False

        if not all(block.is_valid(self) for block in self.blocks):
            return False

        return True

    def get_transaction(self, transaction_id):
        """
        Returns the Transaction object with the matching transaction_id from the Blockchain
        :param transaction_id: `bytes`
        :return: `Transaction`
        """
        for block in self.blocks:
            for transaction in block.transactions:
                if transaction.id == transaction_id:
                    return transaction
        raise KeyError(f"There is no transaction with transaction id {transaction_id} in this blockchain! sorry :(")

    def get_output(self, transaction_id, output_id):
        """
        Returns the TransactionOutput object with the matching transaction_id and output_id from the Blockchain
        """
        transaction = self.get_transaction(transaction_id)
        return transaction.outputs[output_id]

    def is_output_used(self, transaction_id, output_id):
        """
        Returns whether or not a certain JIMCoin output has been used yet or not
            Ignore usages within the same transaction - they are impossible and meaningless
        :param transaction_id: `bytes` - the transaction in which the interesting output appears
        :param output_id: `int`        - the index of the interesting output inside the transaction
        :return: `bool`
        """
        for block in self.blocks:
            for transaction in block.transactions:
                if transaction.id == transaction_id:
                    continue

                for input_ in transaction.inputs:
                    if input_.transaction_id == transaction_id and input_.output_id == output_id:
                        return True
        return False

    def get_jimcoin_amount(self, public_key):
        """
        Returns the amount of available coins a given user has.
        User - means a public key. The identification of a user of course is by its keys - whether or not he can decrypt the outputs of Transactions
            that are sent to him :)
        :param public_key: `str`
        :return: `int`
        """
        amount = 0
        for block in self.blocks:
            for transaction in block.transactions:
                for output_id, output in enumerate(transaction.outputs):
                    if output.locking_public_key == public_key and not self.is_output_used(transaction.id, output_id):
                        amount += output.amount
        return amount

    def get_next_target(self, top_index=None):
        blocks = self.blocks

        if not blocks:
            return BLOCK.INITIAL_TARGET

        if top_index is not None:
            blocks = blocks[:top_index]

        last_blocks = blocks[-BLOCK.TARGET_CALCULATION_SAMPLE_COUNT:]
        expected_mining_time = len(last_blocks) * BLOCK.EXPECTED_MINING_TIME
        actual_mining_time = (datetime.now() - last_blocks[0]).total_seconds()
        old_target = blocks[-1].header.target

        return old_target * (expected_mining_time / actual_mining_time)

    @staticmethod
    def get_block_reward(block_index):
        return COIN.INITIAL_BLOCK_REWARD / (COIN.REWARD_DECREASE_RATE * (block_index // int(COIN.BLOCK_COUNT_UNTIL_DECREASING_REWARD)))
