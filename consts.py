class RSA:
    SIGNATURE_LENGTH = 128

    class KEY:
        class PUBLIC:
            MAX_LENGTH = 400


class HASH:
    METHOD = 'SHA-256'
    LENGTH = 32


class TRANSACTION:
    MAX_OUTPUT_COUNT = 128
    MAX_INPUT_COUNT = 128

    class INPUTS:
        NULL_OUTPUT_ID = 0xffffffff


class BLOCK:
    MAX_TRANSACTIONS = 2048
    EXPECTED_MINING_TIME = 600  # seconds
    TARGET_CALCULATION_SAMPLE_COUNT = 10

    NONCE_BYTE_SIZE = 32

    TARGET_BYTE_SIZE = 32
    INITIAL_TARGET = (2 ** (TARGET_BYTE_SIZE * 8)) - 1  # the largest possible target! meaning any hash would be valid...

    class CHAIN:
        MAX_LENGTH = 500_000_000


class COIN:
    INITIAL_BLOCK_REWARD = 1024.0
    BLOCK_COUNT_UNTIL_DECREASING_REWARD = 100
    REWARD_DECREASE_RATE = 0.5
