from structures.transaction.input import TransactionInput
from structures.transaction.output import TransactionOutput
from structures.transaction.transaction import Transaction


def pay(blockchain, amount, miner_fee, sender_public_key, sender_private_key, receiver_public_key, communicator):
    sender_jimcoin_amount = blockchain.get_jimcoin_amount(sender_public_key)
    spent_amount = amount + miner_fee
    if sender_jimcoin_amount < spent_amount:
        raise ValueError(
            f"Sender does not own enough JIMCoin to perform the transaction! They only have {sender_jimcoin_amount} and not {spent_amount}"
        )

    my_spendable_outputs = []  # a list of tuples (transaction_id, output_id)
    my_spendable_amount = 0
    for block in blockchain.blocks:
        for transaction in block.transactions:
            for output_id, output in enumerate(transaction.outputs):
                if output.locking_public_key == sender_public_key:
                    my_spendable_outputs.append((transaction.transaction.id, output_id))
                    my_spendable_amount += output.amount
                    if my_spendable_amount >= spent_amount:
                        break

    remainder_non_spent_outputs = [TransactionOutput(my_spendable_amount - spent_amount)] if my_spendable_amount > spent_amount else []
    transaction = Transaction.generate_with_id(
        inputs=[TransactionInput.unlock_from_output(blockchain, transaction_id, output_id, sender_private_key)
                for transaction_id, output_id in my_spendable_outputs],
        outputs=[TransactionOutput(amount, receiver_public_key)] + remainder_non_spent_outputs,
    )

    communicator.publish_transaction(transaction)
