from operator import attrgetter

from structures.block import Block
from consts import BLOCK


def find_perfect_nonce(block):
    for nonce in range(2 ** (BLOCK.NONCE_BYTE_SIZE * 8)):
        block.header.nonce = nonce
        if block.header.is_valid():
            return nonce
    raise KeyError("Could not find any fitting nonce fot this block!")


def mine(public_key, communicator):
    blockchain = communicator.update_blockchain()
    new_transactions = list(sorted([transaction for transaction in communicator.update_transactions() if transaction.is_valid(blockchain=blockchain)],
                                   key=attrgetter('miner_fee'), reverse=True))[:BLOCK.MAX_TRANSACTIONS]

    block = Block.from_transactions(
        target=blockchain.get_next_target(),
        hash_of_previous_block=blockchain.blocks[-1].hash(),
        miner_public_key=public_key,
        block_reward_and_fees=blockchain.get_block_reward(len(blockchain.blocks)) +
                              sum(transaction.get_miner_fee(blockchain) for transaction in new_transactions),
        transactions=new_transactions
    )
    block.header.nonce = find_perfect_nonce(block)
    communicator.publish_block(block)
