from hashlib import sha256 as sha


def calc_hash(bytes_):
    return sha(bytes_).digest()


def best_effort_coupling(list_):
    """
    Take in [10, 20, 30, 40, 50, 60, 70]
    Return [[10, 20], [30, 40], [50, 60], [70]]
    """
    return [list_[i:i + 2] for i in range(0, len(list_), 2)]
