from bytable import Bytable
from merkle_tree.node import MerkleLeaf, MerkleMidpoint
from utils import best_effort_coupling


class MerkleTree:
    def __init__(self, values):
        if not all(isinstance(value, (Bytable, bytes)) for value in values):
            raise TypeError(f"All values of a MerkleTree must be bytes or Bytable-s!")

        values = [value if isinstance(value, bytes) else value.pack() for value in values]

        top_row = [MerkleLeaf(value) for value in values]
        while len(top_row) > 1:
            top_row = [MerkleMidpoint(couple_or_single) for couple_or_single in best_effort_coupling(top_row)]

        self.root_node, = top_row

    @classmethod
    def is_valid_root(cls, root, values):
        return MerkleTree(values).root_node.data == root
