from typing import List, ByteString
from abc import ABCMeta
from utils import calc_hash


class MerkleNode(metaclass=ABCMeta):
    def __init__(self, data: ByteString):
        self.data = data


class MerkleLeaf(MerkleNode):
    pass


class MerkleMidpoint(MerkleNode):
    def __init__(self, children: List[MerkleNode]):
        super(MerkleMidpoint, self).__init__(calc_hash(b''.join([child.data for child in children])))
        self.children = children
