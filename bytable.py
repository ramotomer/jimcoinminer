import time
from datetime import datetime

from dataclasses import dataclass
from rsa.transform import int2bytes, bytes2int
from enum import Enum
import struct
from abc import ABCMeta

from utils import calc_hash

STRUCT_BUILTIN_TYPES = '<>@!=xcbB?hHiIlLqQnNefdspP'
TO_BYTES_PLACE = 0
FROM_BYTES_PLACE = 1


def required_bytes_for(integer):
    return len(int2bytes(integer))


def encode_with_known_length(value, length, to_bytes, raise_if_too_long=False):
    """
    chop the byte sequence or pad it with 0-s, according to the provided length
    """
    as_bytes = to_bytes(value)
    if raise_if_too_long and (len(as_bytes) > length):
        raise AttributeError(f"the byte_sequence supplied is larger than the max_length specified! bytes: {as_bytes} length: {length}")

    return (b'\x00' * (length - len(as_bytes))) + as_bytes[-length:]


def with_length_as_prefix(byte_sequence, required_bytes_for_length):
    """
    Take in a byte sequence, encode its length to bytes, prefix that length in bytes to the sequence itself
    :param byte_sequence: `bytes`
    :param required_bytes_for_length: `int`
    :return: `bytes`
    """
    encoded_length = encode_with_known_length(len(byte_sequence), required_bytes_for_length, int2bytes, True)
    return encoded_length + byte_sequence


def encode_datetime(dt):
    return struct.pack(">LI", int(time.mktime(dt.timetuple())), dt.microsecond)


def decode_datetime(bytes_):
    timestamp, microseconds = struct.unpack(">LI", bytes_)
    date_without_microseconds = datetime.fromtimestamp(timestamp)
    return datetime(
        date_without_microseconds.year,
        date_without_microseconds.month,
        date_without_microseconds.day,
        date_without_microseconds.hour,
        date_without_microseconds.minute,
        date_without_microseconds.second,
        microseconds
    )


class AttributeTypes(Enum):
    INT = 0
    BOOL = 1
    FLOAT = 2
    ASCII_STR = 3
    UTF8_STR = 4
    BYTES = 5
    DATETIME = 6


BYTES_FACTORY_FROM_TYPE = {
    AttributeTypes.INT:       (int2bytes,                            bytes2int),
    AttributeTypes.FLOAT:     (lambda f: struct.pack('>d', f),       lambda b: struct.unpack('>d', b)),
    AttributeTypes.ASCII_STR: (lambda s: s.encode('ascii'),          lambda b: b.decode('ascii')),
    AttributeTypes.UTF8_STR:  (lambda s: s.encode('utf-8'),          lambda b: b.decode('utf-8')),
    AttributeTypes.BOOL:      (lambda b: b'\x01' if b else b'\x00',  lambda b: b == b'\x01'),
    AttributeTypes.BYTES:     (lambda b: b,                          lambda b: b),
    AttributeTypes.DATETIME:  (encode_datetime,                      decode_datetime),
}


class AttributeDescription:
    """
    Describes an attribute of a Bytable object.
    Describes how to turn in into bytes (how long it will be, will the length be variable)

        name - the name of the attribute
        length - how many bytes the packed attribute will take up. None for dynamic
        max_length - if length is not None, max_length should be None. max_length is the longest the length of the attribute will be
    """
    def __init__(self, name, length=None, max_length=None):
        self.name = name
        self.length = length
        self.max_length = max_length

    @property
    def has_length_prefix(self):
        return self.max_length is not None and self.length is None

    def slice_bytes_to_obtain_length(self, bytes_):
        length = self.length
        if self.has_length_prefix:
            length = bytes2int(bytes_[:required_bytes_for(self.max_length)])
            bytes_ = bytes_[required_bytes_for(self.max_length):]
        return bytes_, length


class SingleAttributeDescription(AttributeDescription):
    """
    Describes a non-iterable attribute of a Bytable object.

        type - an AttributeType enum-object. This is used to deduce a best-effort way to turn the attribute into bytes.
        to_bytes - a function that takes in the value of the attribute and returns the byte representation of it
            if to_bytes is not supplied, It is deduced best-effort using the type. (see the BYTES_FACTORY_FROM_TYPE dict)
    """
    def __init__(self, name, type_, length=None, max_length=None):
        super(SingleAttributeDescription, self).__init__(name, length, max_length)
        self.type = type_

        if issubclass(self.type, Bytable):
            self.max_length = self.type.max_length()

        self.__to_bytes = None
        self.__from_bytes = None

    def get_default_packing_method(self):
        """
        returns a tuple (to_bytes, from_bytes)
        :return:
        """
        if self.type in BYTES_FACTORY_FROM_TYPE:
            return BYTES_FACTORY_FROM_TYPE[self.type]
        if issubclass(self.type, Bytable):
            return (lambda bytable: bytable.pack()), self.type.unpack
        raise TypeError(f"Unknown type '{self.type}' for a Bytable Attribute :(")

    @property
    def to_bytes(self):
        return self.__to_bytes or self.get_default_packing_method()[TO_BYTES_PLACE]

    @to_bytes.setter
    def to_bytes(self, value):
        self.__to_bytes = value

    @property
    def from_bytes(self):
        return self.__to_bytes or self.get_default_packing_method()[FROM_BYTES_PLACE]

    @from_bytes.setter
    def from_bytes(self, value):
        self.__from_bytes = value

    def encode(self, value):
        if self.max_length is not None:
            return with_length_as_prefix(self.to_bytes(value), required_bytes_for(self.max_length))
        return encode_with_known_length(value, self.length, self.to_bytes)


@dataclass
class IterableAttributeDescription(AttributeDescription):
    """
    Describes an iterable attribute of a Bytable object.

                                    NOTE!!!!!!!!!!
                    vvvvvvvvvvvvvvv               vvvvvvvvvvvvvvv

        length and max_length here DESCRIBE THE LENGTH OF THE LIST (the item_count) AND NOT the actual length in bytes

        items - a SingleAttributeDescription of the inner items of the iterable
        .
            Note that ALL ITEMS MUST BE WITH THE SAME DESCRIPTION!!!
    """
    def __init__(self, name, items, length=None, max_length=None):
        super(IterableAttributeDescription, self).__init__(name, length, max_length)
        self.items = items

    def encode(self, value):
        encoded = b''.join([self.items.encode(item) for item in value])

        if self.max_length is not None:
            encoded_length = encode_with_known_length(len(value), required_bytes_for(self.max_length), int2bytes, True)
            return encoded_length + encoded
        elif self.length is not None and len(value) != self.length:
            raise AttributeError(f"Using a static length for your iterable attribute '{self.name}' "
                                 f"but it does not match the real length of the list!")
        return encoded
        

class Bytable(metaclass=ABCMeta):
    def __init__(self, *args, **kwargs):
        self._validate_attribute_descriptions()

        if len(args) + len(kwargs) != len(self.attribute_descriptions):
            raise AttributeError("Attribute count does not match attribute descriptions!")

        for arg, description in zip(args, self.attribute_descriptions):
            setattr(self, description.name, arg)

        for name, value in kwargs.items():
            if hasattr(self, name):
                raise AttributeError(f"Attribute '{name}' redefined!!!!")
            if not any(description.name == name for description in self.attribute_descriptions):
                raise AttributeError(f"No such Attribute '{name}'!!!!")
            setattr(self, name, value)

    @classmethod
    def _validate_attribute_descriptions(cls):
        if not hasattr(cls, 'attribute_descriptions'):
            raise AttributeError("Users of the Bytable class must define a class variable named `attribute_descriptions`"
                                 "which is a list of SingleAttributeDescription-s")
        if any(not isinstance(at, AttributeDescription) for at in cls.attribute_descriptions):
            raise AttributeError("Bytable.attribute_descriptions should be a list of AttributeDescription objects")

    @classmethod
    def max_length(cls):
        """
        Returns the largest amount of bytes that the packed Bytable object would take up
        :return: int
        """
        return sum([(ad.length if ad.length is not None else (required_bytes_for(ad.max_length) + ad.max_length))
                    for ad in cls.attribute_descriptions])

    def pack(self):
        self._validate_attribute_descriptions()

        packed = b''
        for attribute_description in self.attribute_descriptions:
            value = getattr(self, attribute_description.name)
            packed += attribute_description.encode(value)
        return packed

    @classmethod
    def unpack(cls, packed):
        cls._validate_attribute_descriptions()

        attribute_values = {}
        for ad in cls.attribute_descriptions:
            packed, length = ad.slice_bytes_to_obtain_length(packed)

            if isinstance(ad, SingleAttributeDescription):
                attribute_values[ad.name] = ad.from_bytes(packed[:length])
                packed = packed[length:]

            elif isinstance(ad, IterableAttributeDescription):
                attribute_values[ad.name] = []
                for i in range(length):
                    packed, length = ad.slice_bytes_to_obtain_length(packed)
                    attribute_values[ad.name].append(ad.items.from_bytes(packed[:length]))
                    packed = packed[length:]

        return cls(**attribute_values)

    def hash(self, times=1):
        """
        Hashes the object's bytes `times` times
        :param times: `int` - how many times to hash the object
        :return: `bytes` - the hashed object
        """
        returned = self.pack()
        for _ in range(times):
            returned = calc_hash(returned)
        return returned

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False

        return self.pack() == other.pack()

    def __repr__(self):
        return f"""<{self.__class__.__name__}({', '.join('{}={}'.format(ad.name, getattr(self, ad.name)) for ad in self.attribute_descriptions)})>"""
